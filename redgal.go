package redgal

import (
	"fmt"

	"github.com/garyburd/redigo/redis"
)

type Options struct {
	Pool   *redis.Pool
	Prefix string
}

type RedGal struct {
	ops Options
}

func New(ops Options) *RedGal {
	this := &RedGal{
		ops: ops,
	}
	if this.ops.Prefix == "" {
		this.ops.Prefix = "redgal"
	}
	return this
}

func (this *RedGal) GetByID(id string) *GalleryModel {
	return getGalleryModel(this, id)
}

func (this *RedGal) RemoveByID(id string) error {
	var conn redis.Conn
	defer this.conn(&conn)()
	exists, _ := redis.Int(rperror(conn.Do("exists", this._kgal(id))))
	if exists == 1 {
		curParentID, _ := redis.String(rperror(conn.Do("hget", this._kgal(id), "parent")))
		if curParentID != "" {
			rperror(conn.Do("zrem", this._kchildren(curParentID), id))
		}
		rperror(conn.Do("del", this._kgal(id)))
	}
	return nil
}

func (this *RedGal) conn(conn *redis.Conn) func() {
	*conn = this.ops.Pool.Get()
	return func() {
		(*conn).Close()
	}
}

func (this *RedGal) _kgal(id string) string {
	return fmt.Sprintf("%s:meta:%s", this.ops.Prefix, id)
}

func (this *RedGal) _kimages(id string) string {
	return fmt.Sprintf("%s:images:%s", this.ops.Prefix, id)
}

func (this *RedGal) _kchildren(id string) string {
	return fmt.Sprintf("%s:children:%s", this.ops.Prefix, id)
}

func (this *RedGal) _kimage(id string) string {
	return fmt.Sprintf("%s:image:%s", this.ops.Prefix, id)
}
