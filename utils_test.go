package redgal

import "testing"

func TestUpdateIDString(t *testing.T) {
	type Case struct {
		InitID, Prefix, ResID string
		Pass, Panic           bool
	}

	cases := []Case{
		{"1", "name", "name-1", true, false},
		{"name-1", "", "1", true, false},
		{"hello-1", "name", "name-1", true, false},
		{"1", "", "1", true, false},
		{"-1", "name", "name-1", false, true},
		{"", "name", "name", false, true},
		{"he@llo-1", "name", "name-1", false, true},
		{"hello-1", "n@me", "name-1", false, true},
	}

	for _, c := range cases {
		func() {
			defer func() {
				if rec := recover(); rec != nil {
					if c.Pass {
						t.Errorf("Case `%v` failed: %v", c, rec)
						t.Fail()
					}
					if !c.Panic {
						t.Errorf("Case `%v` should not panic!", c)
						t.Fail()
					}
				}
			}()

			if resID := updateIDString(c.Prefix, c.InitID); resID != c.ResID {
				t.Errorf("Case `%v` failed: unexpected result: `%s` != `%s`", c, resID, c.ResID)
				t.Fail()
				if c.Panic {
					t.Errorf("Case `%v` should panic!", c)
					t.Fail()
				}
			}
		}()
	}
}
