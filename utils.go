package redgal

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/ullutau/gounidecode/unidecode"
)

func Title4ID(str string) string {
	str = unidecode.Unidecode(str)
	str = strings.ToLower(str)
	str = strings.Replace(str, " ", "-", -1)

	return str
}

func perror(err error) {
	if err != nil {
		panic(err)
	}
}

func rperror(res interface{}, err error) (interface{}, error) {
	if err != nil {
		panic(err)
	}
	return res, nil
}

func marshal(v interface{}) string {
	data, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		panic(err)
	}
	return base64.StdEncoding.EncodeToString(data)
}

func unmarshal(data string, v interface{}) {
	data_, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(data_, v)
	if err != nil {
		panic(err)
	}
}

const (
	rxsName = `[a-z\-0-9]+`
	rxsID   = `[0-9]+`
)

var (
	rxpID   = regexp.MustCompile(`^(` + rxsName + `\-` + rxsID + `|` + rxsID + `)$`)
	rxpName = regexp.MustCompile(`^` + rxsName + `$`)
)

func updateIDString(prefix, id string) string {
	var (
		nid int64
		err error
	)

	if !rxpID.MatchString(id) {
		panic(fmt.Sprint("Malformed ID: ", id))
	}

	if prefix != "" && !rxpName.MatchString(prefix) {
		panic(fmt.Sprint("Malformed Name: ", prefix))
	}

	if nid, err = strconv.ParseInt(id, 10, 64); err == nil {
		if prefix == "" {
			return fmt.Sprint(nid)
		} else {
			return fmt.Sprint(prefix, "-", nid)
		}
	} else {
		spl := strings.Split(id, "-")
		if prefix == "" {
			return fmt.Sprint(spl[len(spl)-1])
		} else {
			return fmt.Sprint(prefix, "-", spl[len(spl)-1])
		}
	}
}
