package redgal

import (
	"encoding/json"
	"testing"

	"bitbucket.org/colorsocean/s2"

	"github.com/garyburd/redigo/redis"
)

func mkgal(rg **RedGal, t *testing.T) func() {
	c, err := redis.Dial("tcp", ":6379")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	err = c.Send("multi")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	err = c.Send("select", 8)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	err = c.Send("flushdb")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	_, err = c.Do("exec")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	*rg = New(Options{
		Conn:   c,
		Prefix: "redgal_test",
	})

	return func() {
		err := c.Close()
		if err != nil {
			t.Error(err)
			t.FailNow()
		}
	}
}

func toJSON(v interface{}) string {
	data, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		panic(err)
	}
	return string(data)
}

func TestCreate(t *testing.T) {
	var rg *RedGal
	defer mkgal(&rg, t)()

	gal := rg.GetByID("")
	if gal.Exists() {
		t.Error("Gallery exists, but should'nt")
	}

	gal.SetTitle("Hello, Redgal!")
	gal.SetTitle("Hello, Redgal!")

	gal.SetID("hello")

	t.Log(toJSON(gal.GetInfo()))

	img1 := gal.AddImage(Image{
		Full: &s2.FileInfo{
			ID:   "idofanimage",
			MIME: "mime/mime",
		},
	})
	t.Log(toJSON(gal.GetInfo()))

	t.Log(toJSON(rg.GetByID("").SetParent(gal.id).GetInfo()))

	t.Log(toJSON(gal.GetInfo()))

	gal.RemoveImage(img1.ID)

	t.Log(toJSON(gal.GetInfo()))
}

func BenchmarkGetInfo(b *testing.B) {
	b.ReportAllocs()
	var rg *RedGal
	defer mkgal(&rg, nil)()

	gal := rg.GetByID("")
	if gal.Exists() {
		b.FailNow()
	}

	gal.SetTitle("Hello, Redgal!")

	gal.SetID("hello")

	for i := 0; i < 1000; i++ {
		gal.AddImage(Image{
			Full: &s2.FileInfo{
				ID:   "idofanimage",
				MIME: "mime/mime",
			},
		})
	}

	b.ResetTimer()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		gal.GetInfo()
	}
	b.StopTimer()
}
