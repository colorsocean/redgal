package redgal

import (
	"fmt"
	"time"

	"github.com/garyburd/redigo/redis"
	"github.com/satori/go.uuid"

	"bitbucket.org/colorsocean/s2"
)

type Image struct {
	ID    string `json:",omitempty"`
	Full  *s2.FileInfo
	Thumb *s2.FileInfo
}

type GalleryInfo struct {
	ID    string
	Title string
	Descr string `json:",omitempty"`

	Created time.Time `json:",omitempty"`

	Cover Image `json:",omitempty"`

	Images   []Image       `json:",omitempty"`
	Children []GalleryInfo `json:",omitempty"`
	Crumbs   []GalleryInfo `json:",omitempty"`
}

type GalleryModel struct {
	id string

	exists bool
	rg     *RedGal
}

func getGalleryModel(rg *RedGal, id string) *GalleryModel {
	gal := &GalleryModel{
		rg: rg,
	}

	gal.id = id
	if gal.id == "" {
		u := uuid.NewV4()
		gal.id = fmt.Sprintf("%x%x%x%x%x", u[:4], u[4:6], u[6:8], u[8:10], u[10:])
	}

	gal.exists = gal.Exists()

	return gal
}

func (this *GalleryModel) Exists() bool {
	var conn redis.Conn
	defer this.rg.conn(&conn)()
	exists, _ := redis.Int(rperror(conn.Do("exists", this.rg._kgal(this.id))))
	return exists == 1
}

func (this *GalleryModel) GetInfo() *GalleryInfo {
	var conn redis.Conn
	defer this.rg.conn(&conn)()

	gi := &GalleryInfo{
		ID: this.id,
	}

	if !this.Exists() {
		return gi
	}

	imagesExists, _ := redis.Int(rperror(conn.Do("exists", this.rg._kimages(this.id))))
	childrenExists, _ := redis.Int(rperror(conn.Do("exists", this.rg._kchildren(this.id))))

	//> Get meta info
	gi.ID, _ = redis.String(rperror(conn.Do("hget", this.rg._kgal(this.id), "id")))
	gi.Title, _ = redis.String(rperror(conn.Do("hget", this.rg._kgal(this.id), "title")))
	gi.Descr, _ = redis.String(rperror(conn.Do("hget", this.rg._kgal(this.id), "descr")))
	cover, _ := redis.String(rperror(conn.Do("hget", this.rg._kgal(this.id), "cover")))
	unmarshal(cover, &gi.Cover)
	created, _ := redis.String(rperror(conn.Do("hget", this.rg._kgal(this.id), "created")))
	unmarshal(created, &gi.Created)

	//> Get all children
	if childrenExists == 1 {
		childrenIDs, _ := redis.Strings(rperror(conn.Do("zrange", this.rg._kchildren(this.id), 0, -1)))
		for _, chID := range childrenIDs {
			giChild := &GalleryInfo{}
			giChild.ID, _ = redis.String(rperror(conn.Do("hget", this.rg._kgal(chID), "id")))
			giChild.Title, _ = redis.String(rperror(conn.Do("hget", this.rg._kgal(chID), "title")))
			gi.Children = append(gi.Children, *giChild)
		}
	}

	thisID := this.id
	for {
		parid, _ := redis.String(rperror(conn.Do("hget", this.rg._kgal(thisID), "parent")))
		if parid != "" {
			thisID = parid

			giChild := &GalleryInfo{}
			giChild.ID, _ = redis.String(rperror(conn.Do("hget", this.rg._kgal(parid), "id")))
			giChild.Title, _ = redis.String(rperror(conn.Do("hget", this.rg._kgal(parid), "title")))
			gi.Crumbs = append(gi.Crumbs, *giChild)
		} else {
			break
		}
	}

	if imagesExists == 1 {
		imgsIDs, _ := redis.Strings(rperror(conn.Do("zrange", this.rg._kimages(this.id), 0, -1)))
		for _, imgID := range imgsIDs {
			img := Image{}
			imgData, _ := redis.String(rperror(conn.Do("get", this.rg._kimage(imgID))))
			unmarshal(imgData, &img)
			gi.Images = append(gi.Images, img)
		}
	}

	return gi
}

func (this *GalleryModel) update(tx func(redis.Conn)) *GalleryModel {
	var conn redis.Conn
	defer this.rg.conn(&conn)()
	if !this.exists {
		rperror(conn.Do("hset", this.rg._kgal(this.id), "id", this.id))
		rperror(conn.Do("hset", this.rg._kgal(this.id), "title", "Title"))
		rperror(conn.Do("hset", this.rg._kgal(this.id), "descr", "Description"))
		rperror(conn.Do("hset", this.rg._kgal(this.id), "cover", marshal(Image{})))
		rperror(conn.Do("hset", this.rg._kgal(this.id), "created", marshal(time.Now().UTC())))
		this.exists = true
	}
	tx(conn)
	return this
}

func (this *GalleryModel) SetID(newID string) *GalleryModel {
	this.update(func(conn redis.Conn) {
		oldid := this.id

		imagesExists, _ := redis.Int(rperror(conn.Do("exists", this.rg._kimages(oldid))))
		childrenExists, _ := redis.Int(rperror(conn.Do("exists", this.rg._kchildren(oldid))))

		//> Rename keys
		perror(conn.Send("multi"))
		perror(conn.Send("renamenx", this.rg._kgal(oldid), this.rg._kgal(newID)))
		if imagesExists == 1 {
			perror(conn.Send("renamenx", this.rg._kimages(oldid), this.rg._kimages(newID)))
		}
		if childrenExists == 1 {
			perror(conn.Send("renamenx", this.rg._kchildren(oldid), this.rg._kchildren(newID)))
		}
		rnmds, _ := redis.Ints(rperror(conn.Do("exec")))
		for i, x := range rnmds {
			if x == 0 {
				panic(fmt.Sprintf("ID `%s` already exists [op: %d]", newID, i))
			}
		}

		//> Update id in DB
		rperror(conn.Do("hset", this.rg._kgal(newID), "id", newID))
		//> Cache new ID in model
		this.id = newID

		//> Update `parent` field
		metas_, _ := rperror(conn.Do("scan", 0, "match", this.rg._kgal("*")))
		metas_ = metas_.([]interface{})[1]
		metas, _ := redis.Strings(metas_, nil)
		for _, m := range metas {
			val, _ := rperror(conn.Do("hget", m, "parent"))
			switch val {
			case nil:
				continue
			case oldid:
				rperror(conn.Do("hset", m, "parent", newID))
			default:
				continue
			}
		}
	})
	return this
}

func (this *GalleryModel) SetTitle(title string) *GalleryModel {
	this.update(func(conn redis.Conn) {
		rperror(conn.Do("hset", this.rg._kgal(this.id), "title", title))
	})
	return this
}

func (this *GalleryModel) SetDescr(descr string) *GalleryModel {
	this.update(func(conn redis.Conn) {
		rperror(conn.Do("hset", this.rg._kgal(this.id), "descr", descr))
	})
	return this
}

func (this *GalleryModel) SetCoverImage(cover Image) *GalleryModel {
	this.update(func(conn redis.Conn) {
		rperror(conn.Do("hset", this.rg._kgal(this.id), "cover", marshal(cover)))
	})
	return this
}

func (this *GalleryModel) SetCover(id string) *GalleryModel {
	this.update(func(conn redis.Conn) {
		cover := this.GetImage(id)
		rperror(conn.Do("hset", this.rg._kgal(this.id), "cover", marshal(cover)))
	})
	return this
}

func (this *GalleryModel) AddImage(image Image) *Image {
	this.update(func(conn redis.Conn) {
		u := uuid.NewV4()
		image.ID = fmt.Sprintf("%x%x%x%x%x", u[:4], u[4:6], u[6:8], u[8:10], u[10:])
		rperror(conn.Do("set", this.rg._kimage(image.ID), marshal(image)))
		rperror(conn.Do("zadd", this.rg._kimages(this.id), 0, image.ID))
	})
	return &image
}

func (this *GalleryModel) GetImage(id string) (image *Image) {
	this.update(func(conn redis.Conn) {
		data, _ := redis.String(rperror(conn.Do("get", this.rg._kimage(id))))
		image = new(Image)
		unmarshal(data, &image)
	})
	return
}

func (this *GalleryModel) RemoveImage(imageID string) *GalleryModel {
	this.update(func(conn redis.Conn) {
		rperror(conn.Do("del", this.rg._kimage(imageID)))
		rperror(conn.Do("zrem", this.rg._kimages(this.id), imageID))
	})
	return this
}

func (this *GalleryModel) SetParent(newParentID string) *GalleryModel {
	this.update(func(conn redis.Conn) {
		newParentExists, _ := redis.Int(rperror(conn.Do("exists", this.rg._kgal(newParentID))))
		if newParentExists == 1 {
			curParentVal, _ := rperror(conn.Do("hget", this.rg._kgal(this.id), "parent"))

			perror(conn.Send("multi"))
			{
				if curParentVal != nil {
					curParent, _ := redis.String(curParentVal, nil)
					perror(conn.Send("zrem", this.rg._kchildren(curParent), this.id))
				}

				perror(conn.Send("zadd", this.rg._kchildren(newParentID), 0, this.id))
				perror(conn.Send("hset", this.rg._kgal(this.id), "parent", newParentID))
			}
			rslts, _ := redis.Ints(rperror(conn.Do("exec")))
			for _, x := range rslts {
				if x == 0 {
					panic(fmt.Sprintf("Shit happend: %#v", rslts))
				}
			}
		}

	})
	return this
}
